package com.geetask.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.geetask.chunked.AbstractStorage;
import com.geetask.chunked.FileStorage;

@Configuration
public class Config {

	@Value("${oss.bucket}")
	public String bucketName = "";
	
	@Value("${oss.accessId}")
	public String accessId = "";
	
	@Value("${oss.accessKey}")
	public String accessKey = "";
	
	@Value("${oss.endpoint}")
	public String endpoint = "";
	
	@Bean
	public AbstractStorage storage() {
		//return new FileStorage();
		return new AliyunStorage(endpoint,bucketName,accessId,accessKey);
	}
}
