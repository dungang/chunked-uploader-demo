import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CompleteMultipartUploadRequest;
import com.aliyun.oss.model.CompleteMultipartUploadResult;
import com.aliyun.oss.model.InitiateMultipartUploadRequest;
import com.aliyun.oss.model.InitiateMultipartUploadResult;
import com.aliyun.oss.model.PartETag;
import com.aliyun.oss.model.UploadPartRequest;
import com.aliyun.oss.model.UploadPartResult;

public class UploadTest {

	public static void main(String[] args) throws FileNotFoundException {
		String endpoint = "http://oss-cn-shenzhen.aliyuncs.com";
		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录
		// https://ram.console.aliyun.com 创建RAM账号
		String accessKeyId = "LTAI0uwtOYMmZcrf";
		String accessKeySecret = "7do2U8f2P6LR4WcxgextQbg8covFuR";
		String bucketName = "nda-booking";
		String key = "test.jpg";
		// 创建OSSClient实例
		ClientConfiguration config = new ClientConfiguration();
		config.setCrcCheckEnabled(false);
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret, config);
		InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, key);
		InitiateMultipartUploadResult result = ossClient.initiateMultipartUpload(request);
		String uploadId = result.getUploadId();

		List<PartETag> partETags = new ArrayList<PartETag>();
		InputStream instream = new FileInputStream(new File("D:\\test.jpg"));
		UploadPartRequest uploadPartRequest = new UploadPartRequest();
		uploadPartRequest.setBucketName(bucketName);
		uploadPartRequest.setKey(key);
		uploadPartRequest.setUploadId(uploadId);
		uploadPartRequest.setInputStream(instream);
		// 设置分片大小，除最后一个分片外，其它分片要大于100KB
		uploadPartRequest.setPartSize(1 * 1024 * 1024);
		// 设置分片号，范围是1~10000，
		uploadPartRequest.setPartNumber(1);
		UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
		partETags.add(uploadPartResult.getPartETag());
		System.out.println(partETags);
		Collections.sort(partETags, new Comparator<PartETag>() {
			public int compare(PartETag p1, PartETag p2) {
				return p1.getPartNumber() - p2.getPartNumber();
			}
		});
		CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucketName,
				key, uploadId, partETags);
		CompleteMultipartUploadResult results = ossClient.completeMultipartUpload(completeMultipartUploadRequest);
		System.out.println(results.getRequestId());

	}

}
